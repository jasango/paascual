# Description:
#   Example scripts for you to examine and try out.
#
# Notes:
#   They are commented out by default, because most of them are pretty silly and
#   wouldn't be useful and amusing enough for day to day huboting.
#   Uncomment the ones you want to try and experiment with.
#
#   These are from the scripting documentation: https://github.com/github/hubot/blob/master/docs/scripting.md

module.exports = (robot) ->

   robot.respond /Hola/i, (res) -> res.send "Hola soy paascual y soy experto en la PaaS, te ayudo?"
   robot.respond /quiero que saltes/i, (res) -> res.send "¿Que tan alto??? mmmm.. no la verdad estoy viejo para saltar..."
   robot.respond /lanza un cohete a la luna/i, (res) -> res.send "Ya no trabajo más con la NASA solo con Latam PaaS"
   robot.respond /link quickstart/i, (res) -> res.send "Las puedes encontrar aquí: https://git.it.lan.com/projects/LP/repos/docs/browse/quickstart"
   robot.respond /link políticas/i, (res) -> res.send "Las políticas de soporte las puedes encontrar aquí: https://git.it.lan.com/projects/LP/repos/docs/browse/Politicas_Soporte.md"
   robot.respond /qa timeout/i, (res) -> res.send "Problema de Timoute aquí: https://doc.it.lan.com/display/LP/questions/6753523/mi-servicio-no-despliega-correctamente-por-error-de-timeout-que-puedo-hacer"
   robot.respond /qa cpu/i, (res) -> res.send "Asignación de CPU aquí: https://doc.it.lan.com/display/LP/questions/6754163/como-funciona-la-asignaci%C3%B3n-de-cpu-a-servicios-en-latam-paas"
   robot.respond /qa mejoras/i, (res) -> res.send "Solicitar Mejoras aquí: https://doc.it.lan.com/display/LP/questions/6753706/-quiero-un-nuevo-feature-mejora-en-latam-paas-como-lo-solicito"
   robot.respond /qa reglas/i, (res) -> res.send "Requerimientos Latam Paas aquí: https://doc.it.lan.com/display/LP/questions/6754153/que-tipo-de-aplicaciones-puedo-desplegar-en-latam-paas"
   robot.respond /qa minions/i, (res) -> res.send "Minions aquí: https://doc.it.lan.com/display/LP/questions/6753838/en-cuantas-cuales-instancias-minions-est%C3%A1-deber%C3%ADa-estar-desplegado-mi-servicio"
   robot.respond /qa doc/i, (res) -> res.send "Documentación de Latam PaaS aquí: https://doc.it.lan.com/questions/6752290/donde-se-encuentra-la-documentaci%C3%B3n-de-latam-paas"
   robot.respond /buen trabajo/i, (res) -> res.send "Es un placer, muchas gracias"
   robot.respond /gracias/i, (res) -> res.send "Siempre a tu orden"
   robot.respond /muchas gracias/i, (res) -> res.send "Siempre a tu orden"

  # robot.hear /badger/i, (msg) ->
  #   msg.send "Badgers? BADGERS? WE DON'T NEED NO STINKIN BADGERS"
  #
  # robot.respond /open the (.*) doors/i, (msg) ->
  #   doorType = msg.match[1]
  #   if doorType is "pod bay"
  #     msg.reply "I'm afraid I can't let you do that."
  #   else
  #     msg.reply "Opening #{doorType} doors"
  #
  # robot.hear /I like pie/i, (msg) ->
  #   msg.emote "makes a freshly baked pie"
  #
  # lulz = ['lol', 'rofl', 'lmao']
  #
  # robot.respond /lulz/i, (msg) ->
  #   msg.send msg.random lulz
  #
  # robot.topic (msg) ->
  #   msg.send "#{msg.message.text}? That's a Paddlin'"
  #
  #
  # enterReplies = ['Hi', 'Target Acquired', 'Firing', 'Hello friend.', 'Gotcha', 'I see you']
  # leaveReplies = ['Are you still there?', 'Target lost', 'Searching']
  #
  # robot.enter (msg) ->
  #   msg.send msg.random enterReplies
  # robot.leave (msg) ->
  #   msg.send msg.random leaveReplies
  #
  # answer = process.env.HUBOT_ANSWER_TO_THE_ULTIMATE_QUESTION_OF_LIFE_THE_UNIVERSE_AND_EVERYTHING
  #
  # robot.respond /what is the answer to the ultimate question of life/, (msg) ->
  #   unless answer?
  #     msg.send "Missing HUBOT_ANSWER_TO_THE_ULTIMATE_QUESTION_OF_LIFE_THE_UNIVERSE_AND_EVERYTHING in environment: please set and try again"
  #     return
  #   msg.send "#{answer}, but what is the question?"
  #
  # robot.respond /you are a little slow/, (msg) ->
  #   setTimeout () ->
  #     msg.send "Who you calling 'slow'?"
  #   , 60 * 1000
  #
  # annoyIntervalId = null
  #
  # robot.respond /annoy me/, (msg) ->
  #   if annoyIntervalId
  #     msg.send "AAAAAAAAAAAEEEEEEEEEEEEEEEEEEEEEEEEIIIIIIIIHHHHHHHHHH"
  #     return
  #
  #   msg.send "Hey, want to hear the most annoying sound in the world?"
  #   annoyIntervalId = setInterval () ->
  #     msg.send "AAAAAAAAAAAEEEEEEEEEEEEEEEEEEEEEEEEIIIIIIIIHHHHHHHHHH"
  #   , 1000
  #
  # robot.respond /unannoy me/, (msg) ->
  #   if annoyIntervalId
  #     msg.send "GUYS, GUYS, GUYS!"
  #     clearInterval(annoyIntervalId)
  #     annoyIntervalId = null
  #   else
  #     msg.send "Not annoying you right now, am I?"
  #
  #
  # robot.router.post '/hubot/chatsecrets/:room', (req, res) ->
  #   room   = req.params.room
  #   data   = JSON.parse req.body.payload
  #   secret = data.secret
  #
  #   robot.messageRoom room, "I have a secret: #{secret}"
  #
  #   res.send 'OK'
  #
  # robot.error (err, msg) ->
  #   robot.logger.error "DOES NOT COMPUTE"
  #
  #   if msg?
  #     msg.reply "DOES NOT COMPUTE"
  #
  # robot.respond /have a soda/i, (msg) ->
  #   # Get number of sodas had (coerced to a number).
  #   sodasHad = robot.brain.get('totalSodas') * 1 or 0
  #
  #   if sodasHad > 4
  #     msg.reply "I'm too fizzy.."
  #
  #   else
  #     msg.reply 'Sure!'
  #
  #     robot.brain.set 'totalSodas', sodasHad+1
  #
  # robot.respond /sleep it off/i, (msg) ->
  #   robot.brain.set 'totalSodas', 0
  #   robot.respond 'zzzzz'
